function c = perc_recall(p, x)
    c = 0;
    if dot(p(1, 1:end-1), x(:, 1)) + p(1, end)*1 > 0
        c = 1;
    end
end