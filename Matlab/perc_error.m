function e = perc_error(p, x, c)
    e = 0;
    for k = 1:size(x, 2)
       if perc_recall(p, x(:,k)) ~= c(1,k)
          e = e+1; 
       end
    end
end