import random

class Perceptron(object):
	# Initializes Perceptron weights to random values between 0 and 1
	# Sets the default bias to index n+1
	def __init__(self, n):
		self.w = [random.random() for w_i in range(n)]
		self.w.append(0)

	# Iterates until there is no error in classifying input x or until maximum number of iterations maxit is reached
	# In every iteration, update function is called to update Perceptron's weights
	def learn(self, x, d, lam, maxit):
		epoch = 0

		while epoch < maxit and self.error(x, d) > 0:
			self.update(x, d, lam)
			epoch += 1

	# Updates the Perceptron's weights based on the output of every point in the dataset
	# If the output with current weights is different than desired output, weights will be changed
	def update(self, x, d, lam):
		for i in range(len(x)):
			sum = self.inner_product(x[i])
			res = int(sum > 0)	
			x_i_tmp = x[i] + [1]
			self.w = [self.w[j] + lam*(d[i] - res)*x_i_tmp[j] for j in range(len(self.w))]

	# Returns the number of wrongly classified inputs
	def error(self, x, d):
		return sum([int(self.recall(x[i]) != d[i]) for i in range(len(x))])

	# Returns the class (0/1) of x with current Perceptron's weights 
	def recall(self, x):
		return int(self.inner_product(x) > 0)

	# Returns the inner product of weights and x
	def inner_product(self, x):
		return sum([self.w[i]*x[i] for i in range(len(x))]) + self.w[-1]