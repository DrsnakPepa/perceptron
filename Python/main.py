from Perceptron import Perceptron
import matplotlib.pyplot as plt

x_1 = [[1, 1],
	   [2, 3],
	   [3, 1]]

x_0 = [[3, 4],
	   [4, 2],
	   [4, 4]]

# Input data set
x = x_1 + x_0

# Desired outputs for x
d = [1, 1, 1, 0, 0, 0]

# Initializes new Perceptron of two inputs (+bias)
p = Perceptron(2)

p.learn(x, d, 0.5, 100)

print("Perceptron weight after training: {}".format(p.w))

for i in range(len(x)):
	y_x_i = p.recall(x[i])
	print("Point {} classified as {} --- {}".format(x[i], y_x_i, "OK" if d[i] == y_x_i else "WRONG"))

plt.plot([x[0] for x in x_1], [x[1] for x in x_1], 'ro', [x[0] for x in x_0], [x[1] for x in x_0], 'b+', [p.w[0]*x_i + p.w[2] for x_i in range(5)])
plt.show()